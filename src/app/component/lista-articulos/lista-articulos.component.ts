import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmaDialogComponent } from 'src/app/utils/confirma-dialog/confirma-dialog.component';
import { ArticleComponent } from '../article/article.component';

export interface Articulos {
  sku: string;
  descripcion: string;
  marca: string;
  proveedor: string;
  departamento: string;
  cSinIVA: number;
  costoCIVA: number;
  margen: number;
  precioV: number;
  estado: number;
}

@Component({
  selector: 'app-lista-articulos',
  templateUrl: './lista-articulos.component.html',
  styleUrls: ['./lista-articulos.component.scss']
})
export class ListaArticulosComponent implements AfterViewInit {
  displayedColumns: string[] = ['sku', 'descripcion', 'marca', 'proveedor', 'departamento', 'cSinIVA', 'costoCIVA', 'margen', 'precioV', 'estado'];
  dataSource: MatTableDataSource<Articulos>;
  listArticulo: Articulos[] = [];

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(private router: Router, private dialog: MatDialog) {

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.listArticulo);
    this.getListArticulos();

  }

  clickInLink(codSap: String) {
    console.log(codSap);
    this.openDialogArticuls(codSap);
  }


  mostrarDialogo(event: any, skuCod: String): void {

  }
  eventoCkick(Sku: String) {
    console.log("entro" + Sku);

   let elementData =  this.getElement(Sku);

    if (elementData?.estado == 1) {
      this.dialog
        .open(ConfirmaDialogComponent, {
          data: `¿Estás seguro que quieres desactivar el articulo?`
        })
        .afterClosed()
        .subscribe((confirmado: Boolean) => {
          if (confirmado) {
            this.updateStatusInList(Sku, 0);
          } else {
            this.updateStatusInList(Sku, 1);
          }
          this.dataSource = new MatTableDataSource(this.listArticulo);
          //this.getListArticulos();
        });
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getElement(sku: String){
    const elementToUpdate = this.listArticulo.find(o => o.sku == sku);
    return elementToUpdate;
  }

  updateStatusInList(sku: String, status: number) {

    const elementToUpdate = this.listArticulo.find(o => o.sku == sku);

    if (elementToUpdate) {
      elementToUpdate.estado = status;
      let index = this.listArticulo.indexOf(elementToUpdate);
      this.listArticulo[index] = elementToUpdate;
    }

  }


  checkIschequed(estado: number) {
    if (estado == 1) {
      return true;
    } else {
      return false;
    }
  }

  getListArticulos() {
    this.listArticulo = [
      { sku: "0000000001", descripcion: "Mayonesa helmans 1", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000002", descripcion: "Mayonesa helmans 2", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000003", descripcion: "Mayonesa helmans 3", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 0 },
      { sku: "0000000004", descripcion: "Mayonesa helmans 4", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000005", descripcion: "Mayonesa helmans 5", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000006", descripcion: "Mayonesa helmans 6", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 0 },
      { sku: "0000000007", descripcion: "Mayonesa helmans 7", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000008", descripcion: "Mayonesa helmans 8", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 0 },
      { sku: "0000000009", descripcion: "Mayonesa helmans 9", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000010", descripcion: "Mayonesa helmans 10", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 1 },
      { sku: "0000000011", descripcion: "Mayonesa helmans 11", marca: "helmans", proveedor: "adelco", departamento: "Abrarrote", cSinIVA: 10000, costoCIVA: 11900, margen: 10, precioV: 13092, estado: 0 }

    ];
    this.dataSource = new MatTableDataSource(this.listArticulo);
  }

  openDialogArticuls(skuData: String) {
    const dialogRef = this.dialog.open(ArticleComponent, {
      height: '28.5rem',
      width: '55rem',
      data: {
        sku: skuData
      }
    });

    dialogRef.afterClosed().subscribe((result: undefined) => {
      if (result !== undefined) {

      }
    });
  }

  returnBack() {
    this.router.navigate(['home'])
  }

}
