import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatSliderChange } from '@angular/material/slider';
import { Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
   
  articuleForm = this.formBuilder.group({
    barCode: ['', [Validators.required]],
    description: ['', [Validators.required]],
    proveedor: ['', [Validators.required]],
    marca: ['', [Validators.required]],
    departamento: ['', [Validators.required]],
    pesoKg: [0, [Validators.required]],
    stock: [0, [Validators.required]],
    costoSI: [0, [Validators.required]],
    costoCI: [0, [Validators.required]],
    precioVenta: [0, [Validators.required]],
    margenVenta: [0, [Validators.required]],
    estado: [0,  [Validators.required]]
  }); 
  

  lstproveedores: proveedores[] = [{id:'1',nombre:'Proveedor 1'}, {id:'2',nombre:'Proveedor 2'}];

  lstdepartamento: departamento[] = [{id:'1',nombre:'Departamento 1'}, {id:'2',nombre:'Departamento 2'}];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  private router:Router, 
  private formBuilder: FormBuilder,
  private dialogRef: MatDialogRef<ArticleComponent>) { }


  ngOnInit() {
    console.log("La data es :" + this.data.sku);

  }

  formatLabel(value: number) {
    console.log(value);
    return value + '%';
  }

  onSlider(event: MatSliderChange){
    this.articuleForm.controls['margenVenta'].setValue(event.value);
  }

  larticulo(){
    this.router.navigate(['articulo']) 
  }
 
  returnBack(){
    this.closeDialog();
    //this.router.navigate(['home']) 
  }

  closeDialog(){
    this.dialogRef.close();
  }
}
export interface proveedores {
  id: String;
  nombre: String;
}
export interface departamento {
  id: String;
  nombre: String;
}