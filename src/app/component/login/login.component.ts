import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginInfo: loginInter = {userName:"", pass:""};
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  loginValidation(){
    console.log(this.loginInfo);
    this.router.navigate(['home'])
  }
}
export interface loginInter {
  userName: String;
  pass: String;
}
